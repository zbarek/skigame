
var canvas = document.getElementById('game-canvas');
var ctx
var keypress;
var gameFrame = 0;
var dorsSucces = 0;
var time;
var timePenalty = 10000;
// init game view
function initGame() {
    ctx = canvas.getContext('2d');
    canvas.width = 800;
    canvas.height = 600;
    time = new Date().getTime();
    animate();

}

// keypress
window.addEventListener('keydown', (e) => {
    keypress = e.code;
})

// keypress
window.addEventListener('keyup', (e) => {
    keypress = null;
})


// player

class Skier {
    constructor() {
        this.x = canvas.width / 2;
        this.y = 0;
        this.spriteWidth = 20;
        this.spriteHeight = 30;
        this.radius = this.spriteHeight / 2;
        this.center = {
            x: (this.x + this.spriteWidth / 2),
            y: (this.y + this.spriteHeight / 2),
        }
        this.speedX = 0;
        this.speedY = 0;
        this.speed = 2;
        this.maxSpeeed = 6;
        this.acceleratioin = 0.2;
    }

    update() {


        if (this.speedX < 0) {
            this.speedX = this.speedX + 0.2;
        }

        if (this.speedX > 0) {
            this.speedX = this.speedX - 0.2;
        }


        if (this.speedY > this.maxSpeeed) {
            this.speedY = this.maxSpeeed;
        }

        if (this.y > 100) {
            this.y = 100
        }

        if (keypress === 'ArrowLeft') {
            this.moveLeft();
        }

        if (keypress === 'ArrowRight') {
            this.moveRight();
        }


        if (keypress === 'ArrowUp') {
            this.moveUp();
        }

        if (keypress === 'ArrowDown') {
            this.moveDown();
        }

        // todo
        this.speedY = this.speedY + this.acceleratioin;
        this.y = this.y + this.speedY;
        this.x = this.x + this.speedX;

        this.center = {
            x: (this.x + this.spriteWidth / 2),
            y: (this.y + this.spriteHeight / 2),
        }

    }

    moveUp() {

        this.speedY = this.speedY - 1;
        if (this.speedY < 0.1) {
            this.speedY = 0.1;

        }
    }

    moveLeft() {
        this.speedX = - Math.abs(this.speedY);
        this.speedY = this.speedY - 0.28;

        if (this.speedY < 2) {
            this.speedY = 2;
        }
    }

    moveRight() {
        this.speedX = Math.abs(this.speedY);
        this.speedY = this.speedY - 0.28;

        if (this.speedY < 2) {
            this.speedY = 2;
        }
    }

    moveDown() {
        this.speedY = this.speedY + this.acceleratioin;
        this.speedX = 0

    }

    draw() {

        ctx.globalAlpha = 1
        ctx.beginPath();
        ctx.arc(this.center.x, this.center.y, this.radius, 0, Math.PI * 2);
        ctx.fill();
        ctx.closePath();
        ctx.stroke();
        ctx.globalAlpha = 1
    }
}

let player = new Skier();


// doors
class Dors {

    constructor() {
        this.x = 0;
        this.y = 0;
        this.spriteWidth = 100;
        this.spriteHeight = 10;
        this.remove = false;
        this.color = 'black';
        this.done = false;
        this.counted = false;
    }

    update() {
        this.y = this.y - player.speedY;

        if (this.y < (0 - this.height)) {
            this.remove = true;
        }



        if (this.x < player.center.x &&
            this.x + this.spriteWidth > player.center.x &&
            player.center.y < this.y + this.spriteHeight &&
            player.center.y > this.y
        ) {
            this.done = true;
            if (!this.counted) {
                dorsSucces++;
                this.counted = true;
            }

        }

        if (this.y < player.center.y) {
            this.color = 'red';
            if (!this.counted) {
                time = time - timePenalty;
                this.counted = true;
            }
        }

        if (this.done) {
            this.color = 'green';
        }
    }

    draw() {
        ctx.globalAlpha = 1
        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.rect(this.x, this.y, this.spriteWidth, this.spriteHeight);
        ctx.fill();
        ctx.closePath();
        ctx.stroke();
        ctx.fillStyle = 'black'
        ctx.globalAlpha = 1
    }
}



let doorsArray = [];
// Game loop
function handleDors() {

    let percentage = 120;
    if (gameFrame % percentage === 0) {

        let quantity = 1;
        for (let i = 0; i < quantity; i++) {
            let dor = new Dors();
            dor.x = (Math.random() * canvas.width)
            if (dor.x < 200) {
                dor.x = 200;
            }

            if (dor.x > 500) {
                dor.x = 500;
            }
            dor.y = canvas.height;

            doorsArray.push(dor);
        }

    }

    for (let i = 0; i < doorsArray.length; i++) {
        let dors = doorsArray[i];
        dors.update();
        dors.draw();
        if (dors.remove === true) {

            try {
                doorsArray.splice(i, 1)
            } catch (e) {
                console.log(e.message);
            }

        }
    }

}



// Animation loop
function animate() {
    cuurentTime = new Date().getTime();
    timeDiff = cuurentTime - time;
    t = new Date(timeDiff);
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    //ctx.drawImage(background, 0, 0, 626, 417, 0, 0, canvas.width, canvas.height);
    player.update();
    player.draw();
    handleDors();
    gameFrame++;

    ctx.fillText('SCORE:' + dorsSucces, 10, 20);
    ctx.fillText('TIME: ' + t.getMinutes() + ':' + t.getSeconds() + ':' + t.getMilliseconds(), 700, 20);
    requestAnimationFrame(animate);

}

initGame();

